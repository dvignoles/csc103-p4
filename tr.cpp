/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */
}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);

	/* TODO: finish this... */









	vector<char> ascii;
	vector<int> ascii2;

	//create translation tables
	for(int i = 0; i < 256; i++){
		char temp = i;
		ascii.push_back(temp);
		ascii2.push_back(i);
	}


	string interm;
	while(cin>>interm){  //input from standard input
	vector<char> input;

	//tranlate input string into vector<char>
	for(size_t i = 0; i < interm.length(); i++){
		input.push_back(interm[i]);
	}

	if(comp == 1 && del == 1){

	}
	else if(comp == 1){

	}
	else if(del == 1){
		for(size_t i = 0; i < s1.length(); i++){
			ascii2[(unsigned int)s1[i]] = 999;  //set flag value for certain characters
		}

		for(size_t i = 0; i < input.size(); i++){
			if(ascii2[(unsigned int)input[(unsigned char)i]] != 999){ //check for flag
				cout << input[(unsigned char)i];
			}
		}
		cout << endl;

	}
	else{

		for(size_t i = 0; i < s1.length(); i++){
			ascii[(unsigned int)s1[i]] = (unsigned int)s2[i]; //alter tranlsation table
		}

		for(size_t i = 0; i < input.size(); i++){
			input[(unsigned char)i] = ascii[(unsigned int)input[i]];//translate
		}

		for(size_t j = 0; j < input.size(); j++){
		cout << input[(unsigned char)j];
		}
		cout << endl;

	}

 }
	return 0;
}